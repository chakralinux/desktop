pkgname=mpv
pkgver=0.27.0
pkgrel=1
pkgdesc="A movie player and encoder for linux, based on MPlayer/mplayer2"
arch=('x86_64')
license=('LGPL3')
url="http://mpv.io"
depends=('pulseaudio' 'ffmpeg' 'lcms2' 'libdvdread' 'libgl' 'libvdpau'
         'libxinerama' 'libxv' 'libxkbcommon' 'libva'  'libass' 'uchardet' 
	 'wayland' 'desktop-file-utils' 'hicolor-icon-theme' 'v4l-utils' 'openal'
         'xdg-utils' 'lua52' 'libdvdnav' 'libcdio-paranoia' 'libbluray' 'libxss'
         'enca' 'libguess' 'harfbuzz' 'libxrandr' 'rubberband' 'libarchive' 'libcaca')
makedepends=('mesa' 'python3-docutils' 'ladspa' 'x264' 'jack' 'samba' 'waf')
options=('!emptydirs')
provides=('mplayer')
install=${pkgname}.install
source=("https://github.com/mpv-player/${pkgname}/archive/v${pkgver}.tar.gz"
        '0001-opengl-backend-support-multiple-backends.patch')
sha256sums=('341d8bf18b75c1f78d5b681480b5b7f5c8b87d97a0d4f53a5648ede9c219a49c'
            '609e0530f1b0cdb910dcffb5f62bf55936540e24105ce1b2daf1bd6291a7d58a')

prepare() {
  cd ${pkgname}-${pkgver}

  # --opengl-backend: support multiple backends (#4384) (FS#53962)
  patch -Np1 < "${srcdir}"/0001-opengl-backend-support-multiple-backends.patch
}
 
build() {
  cd ${pkgname}-${pkgver}
 
  # disable SDL, since they are just legacy drivers
  waf configure \
    --prefix=/usr \
    --confdir=/etc/mpv \
    --enable-libmpv-shared \
    --enable-cdda \
    --enable-dvb \
    --enable-zsh-comp \
    --enable-alsa \
    --enable-pulse \
    --enable-openal \
    --enable-wayland \
    --enable-gl-wayland \
    --enable-egl-x11 \
    --enable-libsmbclient \
    --enable-dvdread \
    --enable-vdpau \
    --enable-encoding \
    --enable-dvdnav \
    --enable-libarchive
  waf build
}
 
package() {
  cd ${pkgname}-${pkgver}
 
  waf install --destdir="${pkgdir}" 
  
  install -d "${pkgdir}/usr/share/doc/mpv/examples"
 
  # some extra useful stuff "make install" doesn't install
  install -m755 TOOLS/umpv \
    "${pkgdir}"/usr/bin/umpv
  install -m644 etc/input.conf \
    "${pkgdir}"/usr/share/doc/mpv/examples
  install -m644 DOCS/{encoding.rst,tech-overview.txt} \
    "${pkgdir}"/usr/share/doc/mpv
}
