pkgbase=avidemux
pkgname=('avidemux-cli' 'avidemux')
pkgver=2.6.20
pkgrel=1
pkgdesc='Simple video editor for cutting, filtering and encoding tasks.'
arch=('x86_64')
license=('GPL')
url="http://fixounet.free.fr/avidemux/"
screenshot="http://upload.wikimedia.org/wikipedia/commons/7/77/Avidemux_Screenshot_Qt.png"
makedepends=('cmake' 'libxslt' 'qt5-base' 'qt5-tools' 'qt5-script' 'qt5-multimedia' 'jack' 'libvorbis' 'sdl' 'libxv' 'alsa-lib' 'lame'
             'faad2' 'faac' 'x264' 'x265' 'libsamplerate' 'opencore-amr' 'yasm' 'mesa' 'libvpx' 'sqlite3' 'xvidcore'
             'pulseaudio' 'libva' 'desktop-file-utils' 'libdca' 'fribidi' 'glu' 'aften' 'dcaenc' 'twolame' 'gettext')
categories=('multimedia')
options=("!makeflags")
source=("http://downloads.sourceforge.net/avidemux/avidemux_${pkgver}.tar.gz")
sha256sums=('03c6cb7fc9eb74688b4fcd5eb654ed7b9c4ffc717a72cc09b08a2d10cdc7ef9f')

prepare() {
  cd ${pkgbase}_${pkgver}
  sed -i 's|../avidemux/qt4|../avidemux/qt4 -DLRELEASE_EXECUTABLE=/usr/bin/lrelease-qt5|' bootStrap.bash
  cp avidemux2.desktop avidemux-qt.desktop
  sed -ri 's|(Name=).*|\1avidemux (Qt)|' avidemux-qt.desktop
  sed -ri 's|(Exec=).*|\1avidemux3_qt5|' avidemux-qt.desktop
}
         
build() {
  cd "${srcdir}/${pkgbase}_${pkgver}"

  # Qt5 requires C++11
  CXXFLAGS="$CXXFLAGS -std=c++11"

  bash ./bootStrap.bash --with-core \
                        --with-cli \
                        --with-plugins
}

package_avidemux-cli() {
  pkgdesc="A graphical tool to edit video (filter/re-encode/split)- command line version"
  depends=('faac' 'faad2' 'fontconfig' 'fribidi' 'jack' 'lame' 'libdca' 
  'pulseaudio' 'libva' 'libxv' 'libvpx' 'libxml2' 'opencore-amr'  'xvidcore'
  'sdl' 'sqlite3' 'x264' 'x265' 'aften' 'dcaenc' 'twolame')
  optdepends=('avidemux: graphical interface')

  cd "${srcdir}/${pkgbase}_${pkgver}"
  (cd buildCli; make DESTDIR="${pkgdir}" install)
  (cd buildCore; make DESTDIR="${pkgdir}" install)
  (cd buildPluginsCLI; make DESTDIR="${pkgdir}" install)
  (cd buildPluginsCommon; make DESTDIR="${pkgdir}" install)
  
  install -D -m644 "${srcdir}/${pkgbase}_${pkgver}/avidemux_icon.png" "${pkgdir}/usr/share/pixmaps/avidemux.png" 
  install -D -m644 "${srcdir}/${pkgbase}_${pkgver}/man/avidemux.1" "${pkgdir}/usr/share/man/man1/avidemux.1" 
}

package_avidemux() {
  pkgdesc="A graphical tool to edit video (filter/re-encode/split)"
  depends=("avidemux-cli=$pkgver" 'qt5-base' 'mesa')

  cd "${srcdir}/${pkgbase}_${pkgver}"
  
  (cd buildQt5; make DESTDIR="${pkgdir}" install)
  (cd buildPluginsQt5; make DESTDIR="${pkgdir}" install)

  install -D -m644 avidemux2.desktop "${pkgdir}/usr/share/applications/avidemux.desktop"
  sed -i 's|2_gtk|3_qt5|' "${pkgdir}/usr/share/applications/avidemux.desktop"
  sed -i 's|avidemux2|avidemux3|' "${pkgdir}/usr/share/applications/avidemux.desktop"
}
