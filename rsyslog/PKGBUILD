pkgname=rsyslog
pkgver=8.27.0
pkgrel=1
pkgdesc="An enhanced multi-threaded syslogd with a focus on security and reliability"
url="http://www.rsyslog.com/"
arch=('x86_64')
license=('GPL3')
depends=('zlib' 'libestr' 'libee' 'json-c' 'systemd' 'liblogging' 'librelp>=1.2.12' 'libfastjson')
makedepends=('postgresql-libs>=8.4.1' 'libmariadbclient' 'net-snmp' 'gnutls'
	     'python3-docutils')
optdepends=('postgresql-libs: PostgreSQL Database Support'
	    'libmariadbclient: MySQL Database Support'
	    'net-snmp'
	    'gnutls')
backup=('etc/rsyslog.conf'
	'etc/logrotate.d/rsyslog')
options=('strip' 'zipman')
source=("http://www.rsyslog.com/files/download/rsyslog/rsyslog-$pkgver.tar.gz"
	'rsyslog.logrotate.d'
	'rsyslog.conf')
sha256sums=('02aefbba59324a6d8b70036a67686bed5f0c7be4ced62c039af6ee694cb5b1fd'
            'f5611ebb23b7001292d9af4e731b2933b21c47452871cfca543f4f498c125a0c'
            '5fd51665ab9a81fbb24773068cb261b8dec073d74082c121633f49b9381d9a3f')

prepare() {
  cd ${srcdir}/${pkgname}-${pkgver}
  sed -i rsyslog.service.in \
    -e 's|rsyslogd -n|rsyslogd -n -i /run/rsyslogd.pid|' \
    -e '/ExecStart=.*$/iPIDFile=/run/rsyslogd.pid'
}

build() {
  cd ${srcdir}/${pkgname}-${pkgver}
  ./configure --prefix=/usr \
              --enable-mysql \
              --enable-pgsql \
              --enable-mail \
              --enable-imfile \
              --enable-snmp \
              --enable-gnutls \
              --enable-inet \
              --enable-imjournal \
              --enable-omjournal \
              --enable-relp \
              --enable-impstats \
              --with-systemdsystemunitdir=/usr/lib/systemd/system
  make
}

package() {
  cd ${srcdir}/${pkgname}-${pkgver}
  make install DESTDIR=${pkgdir}
  install -D -m644 $srcdir/${pkgname}.conf ${pkgdir}/etc/${pkgname}.conf
  install -D -m644 $srcdir/${pkgname}.logrotate.d ${pkgdir}/etc/logrotate.d/${pkgname}
}
