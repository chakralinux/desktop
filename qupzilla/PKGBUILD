_pkgname=QupZilla
pkgname='qupzilla'
pkgver=2.2.2
pkgrel=3
pkgdesc="A very fast open source browser based on WebEngine core"
url="http://www.qupzilla.com"
arch=('x86_64')
license=('GPL3')
depends=('qt5-webengine' 'qt5-x11extras' 'qt5-svg' 'hicolor-icon-theme')
makedepends=('qt5-tools' 'kwallet')
optdepends=('bash-completion: bash completion support'
            'kwallet: kf5 kwallet integration')
install=${pkgname}.install
categories=('network')
screenshot='http://www.qupzilla.com/screens/kde.png'
source=("https://github.com/QupZilla/qupzilla/releases/download/v${pkgver}/${_pkgname}-${pkgver}.tar.xz"
        '0001-branded-speeddial.patch'
        'start-white.png'
        'qupzilla.desktop')
sha1sums=('c432999f823177bdc2b5804c087e47926feb287b'
          '098de53c958923ce6f565db24f432f7c2dcb5953'
          '6ec67993bda3054d4210cc08177b124f873774e3'
          '30af0401f97e6465169bae2089d2a8626e08cadd')

prepare() {
  msg 'Applying browserUI patch...'
  cd ${srcdir}/${_pkgname}-${pkgver}/src/lib/app/
  sed -e 's|"qupzilla:start"|"https://www.chakralinux.org/?welcome"|' \
      -e 's|"showStatusBar", true|"showStatusBar", false|' \
      -e 's|"showMenubar", true|"showMenubar", false|g' -i browserwindow.cpp
  cd ${srcdir}/${_pkgname}-${pkgver}/src/lib/plugins/
  sed -e 's|"background", QString()|"file:///usr/share/pixmaps/start-white.png", QString()|' \
      -e 's|"backsize", "auto"|"backsize", "contain"|' -i speeddial.cpp
  cd ${srcdir}/${_pkgname}-${pkgver}/
  
  patch -Np1 -i ${srcdir}/0001-branded-speeddial.patch
  
  msg 'Applying preference patch'
  cd ${srcdir}/${_pkgname}-${pkgver}/src/lib/preferences/
  sed -e 's|"homepage", QUrl(QSL("qupzilla:start"))|"homepage", QUrl(QSL("https://www.chakralinux.org/?welcome"))|' \
      -e 's|"showStatusBar", true|"showStatusBar", false|g' -i preferences.cpp
  msg 'Applying search engine patch...'
  cd ${srcdir}/${_pkgname}-${pkgver}/src/lib/opensearch/
  sed 's,\?q=\%s\&t=qupzilla,\?q=\%s\&t=chakra,g' -i searchenginesmanager.cpp
}
         
build() {
  export USE_WEBGL="true"
  export KDE_INTEGRATION="true"
  export ENABLE_OPACITY_EFFECT="true"

  cd "${srcdir}/${_pkgname}-${pkgver}/"
  /usr/lib/qt5/bin/qmake
  make

}

package() {
  cd "${srcdir}/${_pkgname}-${pkgver}"
  make INSTALL_ROOT="${pkgdir}" install
  install -Dm 644 "${srcdir}/start-white.png" "${pkgdir}/usr/share/pixmaps/start-white.png"
  
  # zsh completion
  install -Dm644 linux/completion/_$pkgname \
    "$pkgdir/usr/share/zsh/site-functions/_$pkgname"
} 
